#! /usr/bin/env python

import argparse
import tarfile
from pathlib import Path
from astropy.utils import data
from astroquery.eso import Eso

parser = argparse.ArgumentParser(description="""
Download testdata for MASCOT Bookeh test.
""")
parser.add_argument('--eso_username', type=str, help="""
ESO Science Portal Username.
""", default='empty')
parser.add_argument('--date', type=str, help="""
Date of the night of MASCOT images to be downloaded. 
Format: 'YYYY MM DD', e.g. --date='2020 01 28'.
""")
parser.add_argument('--output_dir', type=str, required=True, help="""
Directory where files will be stored. Files will be stored in a sub-directory
YYYY-MM-DD in the given directory.
""")
parser.add_argument('--example_data', action='store_true', help="""
If this switch is set no data is downloaded from the ESO archive.  Instead
a folder mascot_tesdata with example files from 2020-01-31 and 2020-02-02 is 
directly downloaded from the web.  As the ESO archive is sometimes very slow, this is the
fastest way to obtain test data.  If this switch is set the options --date and --username
are meaningless.  The example files will be stored in a subdirectory mascot_testdata
 within output_dir.
""")
args = parser.parse_args()

eso = Eso()
eso_portal_username = args.eso_username
date = args.date
output_dir = args.output_dir
example_data = args.example_data

def query_mascot(date, row_limit=-1):
    """
    dataset_tuple = query_mascot(date)
    
    In:
    ---
    date ... date string (YYYY MM DD) of the night to be queried for MASCOT images
    row_limit ... the maximum number of datasets in the output list (default: -1 -
                  which will get all datasets of the specified night)

    Out:
    ----
    dataset_tuple ... a tuple with the date and dataset ids of the images that can be used
                        to retrive them from the ESOarchive (date_str, dataset_id_list)
    """

    eso.ROW_LIMIT = row_limit
    mascot_table = eso.query_main(column_filters={'instrument': 'MASCOT',
                                                  'night': date})
    dataset_id_col = mascot_table['Dataset ID']

    date_split = date.split()
    date_str = date_split[0]+'-'+date_split[1]+'-'+date_split[2]
    
    return date_str,list(dataset_id_col)


def download_mascot(dataset_tuple, eso_portal_username, output_directory):
    eso.login(eso_portal_username, store_password=False)
    output_directory += '/' + dataset_tuple[0] + '/'
    Path(output_directory).mkdir(parents=True, exist_ok=True)
    eso.retrieve_data(dataset_tuple[1], destination=output_directory)
    return output_directory
    

if example_data:
    print('Retrieving testdata... This may take a while...')
    dl_filename = data.download_file('http://www.sc.eso.org/~eherenz/testdata/mascot_testdata.tar.gz',
                                     show_progress=True)
    print(dl_filename)
    print('Unzipping testdata to '+output_dir+'/mascot_testdata/ ...')
    tar = tarfile.open(dl_filename)
    Path(output_dir+'/mascot_testdata/').mkdir(parents=True, exist_ok=True)
    tar.extractall(path=output_dir)
    print('Done! Testdata available in '+output_dir+'/mascot_testdata/')
else:
    print('Downloading MASCOT data from the ESO archive (this takes ~15 minutes)...')
    if eso_portal_username == 'empty':
        print('No ESO Portal Username given... Abort!')
        quit()
    else:
        print('Requested date (YYYY MM DD): '+date)
        dataset_tuple = query_mascot(date, row_limit=-1)
        print('Number of files for '+date+' in archive: '+str(len(dataset_tuple[1])))
        final_output_dir = download_mascot(dataset_tuple, eso_portal_username,
                                           output_dir)
        print('Done!  MASCOT files for '+date+ ' downloaded to '+final_output_dir)
        
        
