# Interactive visualisation of ESO/MASCOT all-sky images with bokeh #

A demo with python bokeh of an interactive visualisation of all sky images taken with the [MASCOT camera](https://eso.org/sci/facilities/paranal/instruments/mascot.html) at the platform of the ESO/Paranal observatory.  An animated picture is worth more than a picture is more worth than thousand words:

![Animated GIF showing MASCOT Bokeh](./mascot_demo.gif)


The above runs in a webbrowser, nice!  Now you want to try it
yourself... right?  Go ahead...

## Try it yourself! ##

Other than python >= 3.7, you only need [astropy](https://www.astropy.org/) (>=4.0) and [bokeh](https://docs.bokeh.org/en/latest/) (>=1.4.0).  I recommend [miniconda and the astroconda  channel](https://astroconda.readthedocs.io/en/latest/) as an optimal astronomic python environment, but your mileage may vary:

	conda install -n astroconda bokeh

### Clone this repository ###
	
	git clone git@bitbucket.org:Knusper2000/mascot_demo.git
	cd mascot_demo

### Download testdata ###

1. If you just want to have a night to play with, you can fetch two demo nights from my server using the included script `query_and_dl.py`:

		query_and_dl.py --example_data --output_dir=/path/to/wherever
	 
   This will store a series of FITS images from two nights (2020-01-31 & 2020-02-02, ca. 220 MB) in the directory `/path/to/whatever/mascot_testdata/`.
	
2. If you want to visualise a specific night, then you can grab that night from the ESO archive.  However, this requires you to have an ESO UserPortal account (which, in any case, is a good thing to have). As an example, here is how to download the images from 2020-02-28 (a particularly nice night with lots of laser action):
   
		./query_and_dl.py --output_dir=/path/to/wherever --eso_username your_username --date='2020 02 28'
   
   This also requires you to have the [`astroquery`](https://astroquery.readthedocs.io/en/latest/) module installed (`conda install astroquery`), which is also generally a good thing to have ([Ginsburg et al. 2019, AJ 157, 98](https://doi.org/10.3847/1538-3881/aafc33)). Due to the fact that the ESO archive needs to find the files in the archive and serve them for you, it will take some time before the actual downloading starts.  The whole process takes about 10 - 2- minutes or so (total amounf of files to be downloaded is ca. 100 MB - more for winter nights, less for summer nights).
(Note: Due to a [strange bug](https://github.com/astropy/astroquery/issues/1357) in astroquery the output will look rather noisy on the shell - just ignore this, grab a cup of tea or hot chocolate and wait until its done.)
   

### Run the thing ###

So you cloned the repository and you have downloaded the testdata?  Then you are ready to go:
	
	bokeh serve mascot_demo.py --args /path/to/wherever/2020-02-28/	

The last part of this command, of course, is the path to the particular night you want to visualise.  Now point your webbrowser (firefox or chrome/[chromium](https://www.chromium.org/Home)) to http://localhost:5006/mascot_demo and
play with the slider to scroll through the night and adjust the cuts or change the colormap with the drop-down menus.
	
## Questions, Feedback, or Bugs? ##

Send an email to eherenz@eso.org or file an issue in the issue tracker.


## License ##

	"THE BEER-WARE LICENSE":
	eherenz@eso.org wrote the code in this repository.  As long as you retain this notice you
	can do whatever you want with this stuff. If we meet some day, and you think
	this stuff is worth it, you can buy me a beer in return.   ECH.
