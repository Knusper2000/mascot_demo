import sys
from glob import glob as g
from astropy.io import fits
from astropy.visualization import PercentileInterval, MinMaxInterval
# bokeh version 1.4.0 --- https://bokeh.org/
from bokeh.models import ColumnDataSource, LinearColorMapper, ColorBar
from bokeh.models.widgets import Select, Slider, TextInput
from bokeh.plotting import figure, output_file, show
from bokeh.layouts import column, row
from bokeh.io import curdoc

# run with 'bokeh serve bokeh_test.py --args ~/work/test/2020-01-28/'
testfile_directory = sys.argv[1]

# prepare data
test_fitsfile_list = g(testfile_directory+'*')
test_fitsfile_list.sort() # sorted by time since YYYY-MM-DDTHH:MM:SS.SSS
                          # in filename 
test_data_list = [fits.getdata(filename)
                  for filename in test_fitsfile_list]
source = ColumnDataSource(data={'image': [test_data_list[0][:,:]]})

percentile_intervall_start = 95
scale_option_list = ['manual','90%', '92%' ,'95%','99%','99.5%','min max']
color_mappers_list = ['Greys256', 'Inferno256', 'Magma256', 'Plasma256', 'Viridis256',
                      'Cividis256', 'Turbo256']
start_intervall = PercentileInterval(percentile_intervall_start)
start_limits = start_intervall.get_limits(test_data_list[0])

# set up the initial plot
color_mapper = LinearColorMapper(palette=color_mappers_list[0],
                                 low=start_limits[0], high=start_limits[1])
plot = figure(x_range=(0,1), y_range=(0,1), toolbar_location=None, title='MASCOT')
image = plot.image(image='image', color_mapper=color_mapper,
                   dh=1.0, dw=1.0, x=0, y=0, source=source)
slider = Slider(start=0, end=len(test_data_list), step=1, value=0)
scale_select = Select(title='Scale Range', value='95%',
                      options=scale_option_list[::-1])
low_text_input = TextInput(value=str(start_limits[0]), title='Low', disabled=True)
high_text_input = TextInput(value=str(start_limits[1]), title='High', disabled=True)
color_mapper_select = Select(title='colormap', value=color_mappers_list[0],
                             options=color_mappers_list)

# interactivity
def update_image(attr, old, new):
    ''' update the image according to slider position '''
    source.data = {'image': [test_data_list[slider.value][:,:]]}

    
def update_scale_option(attr, old, new):
    ''' update the scaling method according to dropdown '''
    scale_option = scale_select.value
    if scale_option == 'manual':
        low_text_input.update(disabled=False)
        high_text_input.update(disabled=False)
    else:
        low_text_input.update(disabled=True)
        high_text_input.update(disabled=True)
        
    if scale_option == 'min max':
        limits = MinMaxInterval().get_limits(test_data_list[slider.value])
        color_mapper.update(low=limits[0], high=limits[1])
    if scale_option.endswith('%'):
        scale_value = float(scale_option[:-1]) 
        pintervall = PercentileInterval(scale_value)
        limits = pintervall.get_limits(test_data_list[slider.value])
        color_mapper.update(low=limits[0], high=limits[1])

    if scale_option == 'min max' or scale_option.endswith('%'):
        low_text_input.update(value=str(round(limits[0], 3)))
        high_text_input.update(value=str(round(limits[1], 3)))

        
def change_low_cut(attr, old, new):
    ''' change the lowest value to be displayed '''
    try:
        new_value = float(new)
    except ValueError:
        # prevent entering a string
        new_value = old
        low_text_input.update(value=str(round(float(new_value), 3)))

    color_mapper.update(low=new_value)

    
def change_high_cut(attr, old, new):
    ''' change the highest value to be displayed '''
    try:
        new_value = float(new)
    except ValueError:
        # prevent entering a string
        new_value = old
        high_text_input.update(value=str(round(float(new_value), 3)))

    color_mapper.update(high=new_value)

def change_colormapper(atrr, old, new):
    color_mapper.update(palette=new)
    
slider.on_change('value', update_image, update_scale_option)
scale_select.on_change('value', update_scale_option, update_image)
low_text_input.on_change('value_input', change_low_cut)
high_text_input.on_change('value_input', change_high_cut)
color_mapper_select.on_change('value', change_colormapper)

# create the output document to be served by 'bokeh serve bokeh_test.py'
plot_width = 610
input_width = 155
out_doc = curdoc()
out_doc.add_root(row(column(plot, slider, width=plot_width),
                     column(scale_select, low_text_input, high_text_input,
                            color_mapper_select,  width=input_width)))


